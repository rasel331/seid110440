<?php
include_once"../../../../vendor/autoload.php";
use App\BITM\SEIP50\Hobby\Hobby;
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$id=$_GET['id'];

$hobby=new Hobby();
$OneHobby=$hobby->edit($id);

?>

<html>
    <head>
        <title>Create | Hobby</title>
    </head>
    <body>
        <fieldset>
            <legend>
               Update Your Hobbies
            </legend> 
            <form action="update.php" method="POST">
                <label>Enter Your Favorite Hobby</label><br/>
                <input type="checkbox" name="hobby[ ]" value="Gardening">Gardening<br/>
                <input type="checkbox" name="hobby[ ]" value="Football">Football<br/>
                <input type="checkbox" name="hobby[ ]" value="Coding">Coding<br/>
                <input type="submit" value="Submit">
            </form>
        </fieldset>
    </body>
</html>