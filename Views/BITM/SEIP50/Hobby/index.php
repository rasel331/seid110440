<a href="create.php">Create</a>
<?php
error_reporting(E_ALL ^ E_DEPRECATED);
//include_once"../../../../Src/BITM/SEIP50/Mobile/Mobile.php";
include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP50\Hobby\Hobby;
use App\BITM\SEIP50\Utility\Utility;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$obj = new Hobby();

$AllHobbies = $obj->index($data='');
$dbg = new Utility();
//$dbg->debug($AllMobiles);
?>
<table border="1">
    <tr>
        <th>
            SL
        </th>
        <th>
            ID
        </th>
        <th>
            Hobby
        </th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($AllHobbies) && !empty($AllHobbies)) {
        $serial = 0;
        foreach ($AllHobbies as $OneHobby) {
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td><?php echo $OneHobby['id'] ?></td>
                <td><?php echo $OneHobby['hobby'] ?></td>                 
                <td>
                    <a href="show.php?id=<?php echo $OneHobby['id'] ?>">Show Details</a>  |
                    <a href="edit.php?id=<?php echo $OneHobby['id'] ?>">Edit</a>  |
                    <a href="delete.php?id=<?php echo $OneHobby['id'] ?>">Delete</a> 


                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="3">
                <?php echo "No avilable Data"; ?>
            </td>
        </tr>
    <?php 
    }
    ?>
</table>
<?php
//$dbg->debug($data);
?>